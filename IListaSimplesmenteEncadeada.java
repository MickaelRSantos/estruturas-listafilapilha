public interface IListaSimplesmenteEncadeada {
	public void addElemento(Object elemento);
	public void addElemento(Object elemento, int pos);
	public Object obter(Object elemento);
	public Object obterDaPosicao(int pos);
	public void removerDaPosicao(int pos);
	public void limpar();
	public int getTamanho();
}