public class PilhaCheiaException extends Exception {

    @Override
    public String toString() {
        return "A Pilha está cheia";
    }
}
