public class PilhaEncadeada extends ListaSimplesmenteEncadeada {
	No inicio;
	No topo;
	int total;
	
	
	@Override
	public void addElemento(Object o){
		No novo = new No(o);
		if(this.estaVazia()) {
			this.inicio = this.topo = novo;
		}else {			
			this.topo.setProximo(novo);
			this.topo = novo;
		}
		total++;
	}
	
	@Override
	public Object obter(Object elemento) {
		No atual = this.inicio;
		while(atual != null) {
			if(atual.getInfo().equals(elemento)) {
				return elemento;
			}
			atual = atual.getProximo();
		}
		return "Não encontrado";
	}
	
	public boolean estaVazia() {
		return total == 0;
	}
	
	@Override
	public int getTamanho() {
		return total;
	}
	
	@Override
	public boolean buscar(Object o){
		No atual = this.inicio;
		int cont = 0;
		while(atual != null) {
			if(atual.getInfo().equals(o)) {
				cont++;
			}
			atual = atual.getProximo();
		}
		if(cont > 0)
			return true;
		return false;
	}

	public No topo() {
		if(this.estaVazia()) {
			return null;
		}
		return (No) topo.getInfo();
	}
	
	public boolean desempilhar() {
		if(this.estaVazia()) {
			return false;
		}else if(total==0) {
			this.inicio = this.topo = null;
			this.total = 0;
			return true;
		}
		else {
			No temp = null;
			temp = this.obtemNo(total-2);
			topo = temp;
			topo.setProximo(null);
			--total;
			return true;
		}
	}
	
	private boolean posOcupada(int pos) {
		return pos >= 0 && pos < this.total;
	}


	private No obtemNo(int pos) {
		if(!this.posOcupada(pos)) {
			throw new IllegalArgumentException("Posicao não existe!");
		}
		No atual = inicio;
		for(int i = 0; i< pos; i++) {
			atual = atual.getProximo();
		}
		return atual;
	}
	
	@Override
	public String toString() {
		if(this.total == 0){
            return "Pilha Encadeada[]";
        }
        StringBuilder b = new StringBuilder("Pilha Encadeada[");
        No atual = inicio;
       do {
            b.append(atual.getInfo());
            b.append(", ");
            atual = atual.getProximo();
        }while(atual != null);
        b.append("]");

        return b.toString();
	}

}
