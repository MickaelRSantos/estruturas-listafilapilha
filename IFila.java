public interface IFila<T> {
    public void enfileirar(T valor) throws FilaCheiaException;
    public void desenfileirar() throws FilaVaziaException;
    public T inicio() throws  FilaVaziaException;
    public boolean estaVazia();
    public boolean estaCheia();
}