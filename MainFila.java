public class MainFila {
    //Inverte Fila com Pilha
    public static Fila inverteFila(Fila f1){
        Pilha<Object> pilha = new Pilha<>(f1.getTamanho());
        Object aux = null, aux1 = null;
        for(int i = 0;i <= f1.getTamanho(); i++){
            try {
                aux = f1.inicio();
            } catch (FilaVaziaException e) {}

            try{
                pilha.empilhar(aux);
            } catch (PilhaCheiaException e) {}

            try{
                f1.desenfileirar();
            } catch (FilaVaziaException e) {}
        }
        for(int i = 0; i<= pilha.getTamanho(); i++){
            try{
                aux1 = pilha.topo();
            } catch (PilhaVaziaException e) {}

            try{
                f1.enfileirar(aux1);
            } catch (FilaCheiaException e) {}

            try{
                pilha.desempilhar();
            } catch (PilhaVaziaException e) {}
        }
        return f1;
    }

    public static void imprimeInverteFila(Fila f){
        System.out.println("Fila invertida" + inverteFila(f));
    }


    //Filas Iguais
    public static boolean iguaisFila(Fila f1, Fila f2){
        Fila f1aux = f1;
        Fila f2aux = f2;
        int cont = 0;
        if(f1.getTamanho()!= f2.getTamanho()){
            return false;
        }else{
            for(int i = 0; i< f1aux.getTamanho(); i++){
                try {
                    if (f1aux.inicio().equals(f2aux.inicio())) {
                        cont++;
                        f1aux.desenfileirar();
                        f2aux.desenfileirar();
                    }else{
                        i = f1aux.getTamanho();
                    }
                }catch (FilaVaziaException e){
                    System.out.println(e.toString());
                }
            }

        }
        if(cont == f1.getTamanho())
            return true;

        return false;
    }

    public static void imprimeIguaisFila(Fila f1, Fila f2){
        System.out.println("As Filas são Iguais? " + iguaisFila(f1, f2));
    }



    //Palindromo Fila errado
    public static boolean testPalindromoFila(String palavra){
        String frase = "";
        frase += palavra.replace(" ", "");
        Fila<Character> fila = new Fila<>(frase.length());
        for(int i=0; i<frase.length(); i++){
            try {
                fila.enfileirar(frase.charAt(i));
            }catch (FilaCheiaException e){
                System.out.println(e.toString());
            }
        }

        inverteFila(fila);
        String palavraAoContrario = "";
        while(!fila.estaVazia()){
            try {
                palavraAoContrario += fila.inicio();
            }catch (FilaVaziaException e) {
                e.printStackTrace();
            }
            try{
                fila.desenfileirar();
            }catch (FilaVaziaException e){
                System.out.println(e.toString());
            }
        }
        if(palavraAoContrario.equalsIgnoreCase(frase))
            return true;

        return false;
    }

    public static void imprimeTestePalindromoFila(String palavra){
        System.out.println(palavra + " é palindromo? " + testPalindromoFila(palavra));
    }



    public static void main(String[] args){
        Fila<Integer> fila = new Fila(3);
        try {
            System.out.println("Enfileirando...");
            fila.enfileirar(6);
        } catch (FilaCheiaException e) {
            System.out.println(e.toString());
        }
        try {
            System.out.println("Enfileirando...");
            fila.enfileirar(7);
        } catch (FilaCheiaException e) {
            System.out.println(e.toString());
        }
        try {
            System.out.println("Enfileirando...");
            fila.enfileirar(8);
        } catch (FilaCheiaException e) {
            System.out.println(e.toString());
        }

        System.out.println("Fila normal " + fila.toString());
        imprimeInverteFila(fila);


        //Palindromo
        imprimeTestePalindromoFila("ararara");
        imprimeTestePalindromoFila("omississimo");
        imprimeTestePalindromoFila("A base do teto desaba");
        imprimeTestePalindromoFila("Adias a data da saida");
        imprimeTestePalindromoFila("batata");
        imprimeTestePalindromoFila("amor a roma");
        imprimeTestePalindromoFila("1122332211");
        imprimeTestePalindromoFila("[[[}}}}}}[[[");

    }
}
