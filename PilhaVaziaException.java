import java.util.Arrays;

public class PilhaVaziaException extends Exception {


    @Override
    public String toString() {
        return "A pilha está vazia";
    }
}
