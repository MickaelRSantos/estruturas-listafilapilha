public class Lista<T> implements ILista<T> {
    private Object[] elemento;
    private int indice;

    Lista(int tamanho){
        this.indice = 0;
        this.elemento = new Object[tamanho];
    }

    Lista(){
        this(10);
    }

    @Override
    public void add(Object o) throws ListaCheiaException {
        if(this.estaCheia()){
            throw new ListaCheiaException();
        }
        elemento[indice] = o;
        indice++;

    }

    @Override
    public void add(Object o, int index) throws ListaCheiaException {
        if(this.estaCheia()){
            throw new ListaCheiaException();
        }
        if(index == 0 && this.estaVazia()){
            this.add(o);
        }
        for(int i = 0; i<=index;i++){
            elemento[i+1] = elemento[i];
        }
        elemento[index] = o;
        indice++;
    }

    @Override
    public void remove(Object o) throws ListaVaziaException, ItemNaoEncontradoException {
        if(this.estaVazia()){
            throw new ListaVaziaException();
        }
        for(int i = 0; i<elemento.length; i++){
            if(elemento[i] == o){
                elemento[i] = null;
                indice--;
            }else{
                throw new ItemNaoEncontradoException();
            }
        }
    }

    @Override
    public void remove(int index) throws ListaVaziaException, ItemNaoEncontradoException {
        if(this.estaVazia()){
            throw new ListaVaziaException();
        }
        for(int i = 0; i<elemento.length; i++){
            if(elemento[i] == elemento[index]){
                elemento[i] = null;
                indice--;
            }else{
                throw new ItemNaoEncontradoException();
            }
        }
    }

    @Override
    public Object find(Object o) throws ItemNaoEncontradoException {
        boolean encontrou = false;
        for(int i = 0; i<elemento.length; i++){
            if(elemento[i] == o) {
                encontrou =true;
            }
        }
        if(encontrou == true){
            System.out.println(o);
            return o;

        }else{
            throw new ItemNaoEncontradoException();
        }
    }

    @Override
    public Object get(int index) throws IndiceForaDosLimitesException {
        boolean encontrou = false;
        if(index>elemento.length){
            throw new IndiceForaDosLimitesException();
        }else{
            for(int j = 0; index<elemento.length; j++){
                if(elemento[j] == elemento[index]){
                    encontrou = true;
                }
            }
        }
        if(encontrou){
            return elemento[index];
        }
        throw new IndiceForaDosLimitesException();
    }

    @Override
    public int count() {
        return indice;
    }

    public boolean estaCheia(){
        return indice == elemento.length;
    }

    public boolean estaVazia(){
        return indice == 0;
    }
}