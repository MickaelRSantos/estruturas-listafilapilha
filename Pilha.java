import java.util.Arrays;

public class Pilha<T> implements IPilha<T> {
    private T[] elementos;
    private int pos;

    public Pilha(int capacidade){
        this.elementos =(T[]) new Object[capacidade];
        this.pos = 0;
    }

    public Pilha(){
        this(10);
    }

    @Override
    public void empilhar(Object valor) throws PilhaCheiaException {
        if(this.estaCheia())
            throw new PilhaCheiaException();
        else{
            elementos[pos] = (T) valor;
            pos++;

        }
    }

    @Override
    public void desempilhar()throws PilhaVaziaException {
        if(this.estaVazia())
            throw new PilhaVaziaException();
        else{
            --pos;
        }
    }

    @Override
    public T topo() throws PilhaVaziaException {
        if(this.estaVazia())
            throw new PilhaVaziaException();
        return elementos[pos-1];
    }

    @Override
    public boolean estaVazia() {
        if(this.pos == 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean estaCheia() {
        if(this.pos >= elementos.length)
            return true;
        return false;
    }

    public int getTamanho(){
        return elementos.length-1;
    }

    @Override
    public String toString() {
        return "["+ Arrays.toString(elementos) +
                ']';
    }
}