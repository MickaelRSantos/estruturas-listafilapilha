public class FilaVaziaException extends Exception {
    @Override
    public String toString() {
        return "A fila está vazia";
    }
}
