public class ListaSimplesmenteEncadeada implements IListaSimplesmenteEncadeada{
	private No inicio;
	private No fim;
	private int total;

	ListaSimplesmenteEncadeada(){
		fim = inicio = null;
		total = 0;
	}

	@Override
	public void addElemento(Object o){
		No novo = new No(o);
		if(this.estaVazia()) {
			this.inicio = this.fim = novo;
		}else {
			this.fim.setProximo(novo);
			this.fim = novo;
		}
		total++;
	}

	@Override
	public void addElemento(Object o, int pos) {
		int aux = pos -1;
		No temp = new No(o);
		if (estaVazia() && pos == 0) {
			this.inicio = this.fim = temp;
		} else {
			No anterior = this.obtemNo(aux);
			No proximo = anterior.getProximo();
			anterior.setProximo(temp);
			temp.setProximo(proximo);
		}
		total++;
	}

	private boolean posOcupada(int pos) {
		return pos >= 0 && pos < this.total;
	}

	private No obtemNo(int pos) {
		if(!this.posOcupada(pos)) {
			throw new IllegalArgumentException("Posicao não existe!");
		}
		No atual = inicio;
		for(int i = 0; i< pos; i++) {
			atual = atual.getProximo();
		}
		return atual;
	}

	private boolean estaVazia() {
		return total == 0;
	}


	public boolean buscar(Object o){
		No atual = this.inicio;
		int cont = 0;
		while(atual != null) {
			if(atual.getInfo().equals(o)) {
				cont++;
			}
			atual = atual.getProximo();
		}
		if(cont > 0)
			return true;
		return false;
	}

	@Override
	public Object obter(Object elemento) {
		No atual = this.inicio;
		while(atual != null) {
			if(atual.getInfo().equals(elemento)) {
				return elemento;
			}
			atual = atual.getProximo();
		}
		return "Não encontrado";

	}

	@Override
	public Object obterDaPosicao(int pos) {
		return this.obtemNo(pos).getInfo();
	}

	@Override
	public void removerDaPosicao(int pos) {
		No temp = null;
		No aux;
		if(pos == 0) {
			inicio = this.inicio.getProximo();
			this.total--;
		}else if(this.total > 0) {
			temp = this.obtemNo(pos-1);
			aux = this.obtemNo(pos);
			temp.setProximo(aux.getProximo());
			this.total--;
		}

	}

	@Override
	public void limpar() {
		this.inicio = null;
		this.fim = null;
		this.total = 0;
	}

	@Override
	public int getTamanho() {
		return total;
	}

	@Override
	public String toString() {
		if(this.total == 0){
			return "Lista Simplesmente Encadeada[]";
		}
		StringBuilder b = new StringBuilder("Lista Simplesmente Encadeada[");
		No atual = inicio;
		do {
			b.append(atual.getInfo());
			b.append(", ");
			atual = atual.getProximo();
		}while(atual != null);
		b.append("]");

		return b.toString();
	}
}