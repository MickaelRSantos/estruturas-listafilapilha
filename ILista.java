public interface ILista<T> {
    void add (Object o) throws ListaCheiaException;
    void add (Object o, int index) throws ListaCheiaException;
    void remove(Object o) throws ListaVaziaException, ItemNaoEncontradoException;
    void remove(int index) throws ListaVaziaException, ItemNaoEncontradoException;
    Object find (Object o) throws ItemNaoEncontradoException;
    Object get (int index) throws IndiceForaDosLimitesException;
    int count();
}