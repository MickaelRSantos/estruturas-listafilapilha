import java.util.Arrays;

public class Fila<T> implements IFila {

    private T[] elementos;
    private int numElementos;
    private int inicio;
    private int fim;
    private int tamanho;


    Fila(int capacidade){
        this.numElementos = capacidade;
        this.elementos =(T[]) new Object[capacidade];
        inicio = fim = tamanho = 0;
    }

    Fila(){
        this(10);
    }


    @Override
    public void enfileirar(Object valor) throws FilaCheiaException {
        if(this.estaCheia())
            throw new FilaCheiaException();
        elementos[fim] = (T)valor;
        fim = (fim+1) % numElementos;
        tamanho ++;
    }

    @Override
    public void desenfileirar() throws FilaVaziaException {
        if(this.estaVazia())
            throw new FilaVaziaException();
        else {
            Object t = elementos[inicio];
            inicio = (inicio + 1) % numElementos;
            tamanho--;
            }

    }

    @Override
    public Object inicio() throws  FilaVaziaException {
        if(this.estaVazia())
            throw new FilaVaziaException();
        else
            return elementos[inicio];
    }

    @Override
    public boolean estaVazia() {
       return (tamanho == 0);
    }

    @Override
    public boolean estaCheia() {
        return (tamanho == numElementos);
    }


    public int getTamanho() {
        return elementos.length;
    }

    @Override
    public String toString() {
        return "["+ Arrays.toString(elementos) + ']';
    }
}