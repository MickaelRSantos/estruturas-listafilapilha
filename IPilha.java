public interface IPilha<T> {
    public void empilhar(T valor) throws PilhaCheiaException;
    public void desempilhar() throws PilhaVaziaException;
    public T topo() throws PilhaVaziaException;
    public boolean estaVazia();
    public boolean estaCheia();
}