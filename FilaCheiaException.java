public class FilaCheiaException extends Exception {

    @Override
    public String toString() {
        return "A Fila está cheia";
    }
}
