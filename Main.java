import java.util.Scanner;

public class Main {
    //Calculo de Binarios com Pilhas
    public static String conversorDecBin(int num){
        Pilha<Integer> pilha = new Pilha<>(50);
        String numBin = "";
        int resto;
        while (num > 0){
            resto = num % 2;
            try{
                pilha.empilhar(resto);
            }catch(PilhaCheiaException e){
            }
            num = num/2;
        }
        while(!pilha.estaVazia()){
            try{
                numBin += pilha.topo();
            }catch(PilhaVaziaException e){
            }
            try{
                pilha.desempilhar();
            }catch(PilhaVaziaException e ){

            }
        }

        return numBin;
    }

    public  static  void imprimeDecBin(int numero){
        System.out.println(numero  + " binario é: " + conversorDecBin(numero));
    }

    //Simbolos Balanceados com Pilhas
    public static boolean verificaBalanceado(String expressao){
        boolean balanceado = true;
        Pilha<Character> pilha = new Pilha(50);
        int i = 0, contIni = 0, contFim = 0;
        char simbolo, top = 0;
        while(i < expressao.length()){
            simbolo = expressao.charAt(i);

            if(INICIO.indexOf(simbolo)> -1){
                contIni++;
                try{
                    pilha.empilhar(simbolo);
                }catch (PilhaCheiaException e){

                }
            }else if (FIM.indexOf(simbolo)> -1){
                if(pilha.estaVazia()){
                   return false;
                }else{
                    contFim++;
                    try {
                        top = pilha.topo();
                    }catch (PilhaVaziaException e){

                    }
                    try{
                        pilha.desempilhar();
                    }catch (PilhaVaziaException e){

                    }
                    if(INICIO.indexOf(top) != FIM.indexOf(simbolo)){
                        return false;
                    }
                }
            }
            i++;
        }
        if(contFim != contIni)
            return false;
        return true;
    }

    final static String INICIO = "([{";
    final static String FIM = ")]}";

    public static void imprimeBalanceado(String expressao){
        System.out.println(expressao + " está balanceada? "+ verificaBalanceado(expressao));
    }

    //Pilhas Iguais
    public static boolean iguaisPilha(Pilha p1, Pilha p2){
        Pilha p1aux = p1;
        Pilha p2aux = p2;
        int cont = 0;
        if(p1.getTamanho()!= p2.getTamanho()){
            return false;
        }else{
            for(int i = 0; i< p1aux.getTamanho(); i++){
                try {
                    if (p1aux.topo().equals(p2aux.topo())) {
                        cont++;
                        p1aux.desempilhar();
                        p2aux.desempilhar();
                    }else{
                        i = p1aux.getTamanho();
                    }
                }catch (PilhaVaziaException e){
                    System.out.println(e.toString());
                }
            }

        }
        if(cont == p1.getTamanho())
            return true;

        return false;
    }

    public static void imprimeIguaisPilha(Pilha p1, Pilha p2){
        System.out.println("As Filas são Iguais? " + iguaisPilha(p1, p2));
    }


    //Palindromo pilha Ok
    public static boolean testPalindromoPilha(String palavra){
        Pilha<Character> pilha = new Pilha<>(palavra.length());
        String frase = "";
        frase += palavra.replace(" ", "");
        for(int i=0; i<frase.length(); i++){
            try {
                pilha.empilhar(frase.charAt(i));
            }catch (PilhaCheiaException e){
                System.out.println(e.toString());
            }
        }
        String palavraAoContrario = "";
        while(!pilha.estaVazia()){
            try {
                palavraAoContrario += pilha.topo();
            }catch (PilhaVaziaException e){
                System.out.println(e.toString());
            }
            try{
                pilha.desempilhar();
            }catch (PilhaVaziaException e){
                System.out.println(e.toString());
            }
        }
        if(palavraAoContrario.equalsIgnoreCase(frase))
            return true;

        return false;
    }

    public static void imprimeTestePalindromoPilha(String palavra){
        System.out.println(palavra + " é palindromo? " + testPalindromoPilha(palavra));
    }


    //Torre de Hanoi Pilha
    public static void torreDeHanoiPilha(int qtd, Pilha<Integer> inicio, Pilha<Integer> fim, Pilha<Integer> aux){

        if(qtd > 0){
           torreDeHanoiPilha(qtd - 1, inicio, aux, fim);
           try{
               aux.empilhar(inicio.topo());
           }catch (PilhaCheiaException e){}
           catch (PilhaVaziaException e) {}
           try{
               inicio.desempilhar();
           }catch (PilhaVaziaException e){}

           System.out.println("--------------");
           System.out.println("Inicio: "+ inicio);
           System.out.println("Final: "+ aux);
           System.out.println("Aux: "+ fim);
           torreDeHanoiPilha(qtd-1, aux, fim, inicio);
       }

    }


    public static void main(String[] args) {
        //Torre de Hanoi Pilha
      /*  Pilha<Integer> inicio = new Pilha<>(3);
        Pilha<Integer> fim = new Pilha<>(3);
        Pilha<Integer> aux = new Pilha<>(3);
        try{
            inicio.empilhar(3);
        }catch (PilhaCheiaException e){}
        try{
            inicio.empilhar(2);
        }catch (PilhaCheiaException e){}
        try{
            inicio.empilhar(1);
        }catch (PilhaCheiaException e){}

        torreDeHanoiPilha(inicio.getTamanho(), inicio, aux, fim);

*/
        //Empilhar Numeros impares e Enfileirar Numeros Paras
       /* Scanner ler = new Scanner(System.in);
        Fila f = new Fila<Integer>(5);
        Pilha p = new Pilha<Integer>(5);
        int valor;
       for(int i = 0; i<10; i++){
            System.out.println("Digite um número: ");
            valor = ler.nextInt();

            if(valor%2 ==0){
                try {
                    System.out.println("Enfileirando...");
                    f.enfileirar(valor);
                } catch (FilaCheiaException e) {
                    System.out.println(e.toString());
                }
            }else{
                try{
                    System.out.println("Empilhando...");
                    p.empilhar(valor);
                }catch(PilhaCheiaException e){
                    System.out.println(e.toString());
                }
            }
        } */
       //Balanceados
      /*  imprimeBalanceado("{A + B[C *(D + A)+B]*A}");
        imprimeBalanceado("(]");
        imprimeBalanceado(")");
        imprimeBalanceado("{{");
        imprimeBalanceado("{{{[]()}}]");
*/

      //Decimal para Binario
 /*       imprimeDecBin(13);
        imprimeDecBin(450);
        imprimeDecBin(0);
        imprimeDecBin(1);
   */

        //Palindromo
       /* imprimeTestePalindromoPilha("ararara");
        imprimeTestePalindromoPilha("omississimo");
        imprimeTestePalindromoPilha("A base do teto desaba");
        imprimeTestePalindromoPilha("Adias a data da saida");
        imprimeTestePalindromoPilha("batata");
        imprimeTestePalindromoPilha("amor a roma");
        imprimeTestePalindromoPilha("1122332211");
*/
        //Filas/Pilhas iguais?
    /*    Pilha<Integer> f1 = new Pilha<>(3);
        Pilha<Integer> f2 = new Pilha<>(3);

        for (int i = 0; i < f1.getTamanho(); i++) {
            System.out.println("Digite um número para Fila f1: ");
            valor = ler.nextInt();
            try {
                f1.empilhar(valor);
            }catch (PilhaCheiaException e){
                System.out.println(e.toString());
            }
        }
        for (int i = 0; i < f2.getTamanho(); i++) {
            System.out.println("Digite um número Fila f2: ");
            valor = ler.nextInt();
            try {
                f2.empilhar(valor);
            }catch (PilhaCheiaException e){
                System.out.println(e.toString());
            }
        }
        imprimeIguaisPilha(f1, f2);
        */
    }
}